package app.minervati.mibneuralyzer.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import app.minervati.mibneuralyzer.R;
import app.minervati.mibneuralyzer.view.CameraView;

public class NeuralyzerFunActivity extends AppCompatActivity {

    private Camera      mCamera = null;
    private Camera      camera;
    private CameraView  mCameraView = null;
    private ImageView   mViewNeuralyzer;
    private AdView mAdView;
    
    private MediaPlayer mediaPlayer;

    private String[]    permissoes = new String[]{
                                        Manifest.permission.CAMERA
                                    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_neuralyzer_fun);

        //Devido a versão 6.0, foi necessário o código abaixo.
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                ActivityCompat.requestPermissions(this, permissoes, 1);
        }

        initComponents();
        
        MobileAds.initialize(this, "ca-app-pub-3575287456847740~3980996911");

        mCameraView = new CameraView(this, mCamera);

        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_view);
        preview.addView(mCameraView);

        mViewNeuralyzer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Camera.Parameters p = mCamera.getParameters();
                mediaPlayer = MediaPlayer.create(NeuralyzerFunActivity.this, R.raw.mib_neuralyzer);
                soundStart();
                sleep(300);
                p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                mCamera.setParameters(p);
                sleep(300);
                p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                mCamera.setParameters(p);
            }

        });

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();

        mAdView.loadAd(adRequest);

    }

    private void initComponents() {
        if(mCamera == null)
            mCamera     = getCameraInstance();
        mViewNeuralyzer = (ImageView) findViewById(R.id.view_neuralyzer);
        mAdView         = (AdView) findViewById(R.id.ad_view);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        camera = null;
    }
    /**
     * Helper method to access the camera returns null if it cannot get the
     * camera or does not exist
     *
     * @return
     */
    private Camera getCameraInstance() {
        camera = null;
        try {
            camera = Camera.open();
        } catch (Exception e) {}
        return camera;
    }

    private void soundStart() {
        mediaPlayer.start();

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.stop();
                mp.release();
            }
        });
    }

    public void sleep(Integer time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
        }
    }
}
