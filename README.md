# README #

##NeuralyzerFun##

* NeuralyzerFun app
* Version 1.0

### NeuralyzerFun app###

É um aplicativo desenvolvido na plataforma Android com intuito de divertir as pessoas que já assistiram um dos filmes do MIB - Homens de Preto.

Quem acompanhou um ou a séria dos filmes MIB - Homens de Preto irá lembrar. Neste, os personagens utilizam um equipamento conhecido como Neuralyzer ,onde este emite um flash que apaga a memória das pessoas.

Mas calma! O app não irá apagar a memória de ninguém, porém é uma ótima forma de se divertir com a turma, dizendo que suas memórias serão apagadas.